<?php

/**
 * Controlador principal del módulo
 */
// Declarar namespace
namespace Drupal\umbral\Controller;

// Importar clases
use Drupal\Core\Controller\ControllerBase; // Controlador base
use Drupal\node\Entity\Node; // Para usar entidades de tipo nodo
use Symfony\Component\HttpFoundation\RedirectResponse; // Para redirecciones
use Drupal\Core\Url;



// Declaración de la clase
class EscuelasController extends ControllerBase
{
    private $table;
    private $routeList;
    private $label;
    private $labelPlural;
    private $routeAdd;
    function __construct()
    {
         $this->table="umbral_escuelas";
         $this->routeList="umbral.escuelaslist";
         $this->label="Escuela";
         $this->labelPlural="Escuelas";
         $this->routeAdd="escuela.form";
     }
    public function list()
    {
        $header_table = array(
           'id'=>   'Id',
          'name' => 'Nombre',
          'clave' => 'Clave',
          'opt' => t('operations'),
          'opt1' => t('operations'),
        );
      $query = \Drupal::database()->select($this->table, 's');
      $query->fields('s', ['id','nombre','clave'])
      ->condition('active','1');
      $results = $query->execute()->fetchAll();
      $rows=[];
      foreach($results as $data){
        $urlAdd=Url::fromRoute($this->routeAdd)->toString();

        $delete = Url::fromUserInput('/admin/schools/delete/'.$data->id);
        $edit   = Url::fromUserInput('/admin/schools/form?id='.$data->id);
      //print the data from table
             $rows[] = [
              'id' =>$data->id,
              'name' => $data->nombre,
              'clave' => $data->clave,
                 \Drupal::l('Delete', $delete),
                 \Drupal::l('Modificar', $edit),
            ];
      }

      $form['#markup'] = '<a href="'.$urlAdd.'" class="button btn-primary">Agregar '.$this->label.'</a><br><br>';
      $form['#title'] = $this->labelPlural;

      $form['table'] = [
            '#type' => 'table',
            '#header' => $header_table,
            '#rows' => $rows,
            '#empty' => t('No users found'),
      ];
       return $form;
    }
    public function listTeacher()
    {
        $header_table = array(
           'id'=>   'Id',
          'name' => 'Nombre',
          'clave' => 'Clave',
          'opt' => t('operations'),
          'opt2' => t(''),
        );
      $query = \Drupal::database()
      ->select($this->table, 't');
      $query->fields('t', ['nombre','clave'])
      ->fields('eP', ['id'])
      ->condition('t.active', '1')
      ->condition('eP.active', '1')
      ->join('umbral_de_escuela_profesor', 'eP', 'eP.escuela_id = t.id');
      $results = $query->execute()->fetchAll();
      $rows=[];
      foreach($results as $data) {
        $urlAddCourse=Url::fromRoute('umbral.cursoslist')->toString();
        $urlAdd=Url::fromRoute("escuela.formteacher")->toString();

        $urlAddCourse  = Url::fromUserInput($urlAddCourse.'?id='.$data->id);
       $urlDelSchool  = Url::fromUserInput('/admin/schools/profesor/delete/'.$data->id);
      //print the data from table
             $rows[] = [
              'id' =>$data->id,
              'name' => $data->nombre,
              'clave' => $data->clave,
                 \Drupal::l('Agregar curso', $urlAddCourse),
                 \Drupal::l('Eliminar escuela', $urlDelSchool),
            ];
      }
      $form['#markup'] = '<a href="'.$urlAdd.'" class="button btn-primary">Agregar  '.$this->label.'</a><br><br>';
      $form['#title'] = $this->labelPlural;

      $form['table'] = [
            '#type' => 'table',
            '#header' => $header_table,
            '#rows' => $rows,
            '#empty' => t('No users found'),
      ];
       return $form;
    }
}
