<?php

/**
 * Controlador principal del módulo
 */


// Declarar namespace
namespace Drupal\umbral\Controller;

// Importar clases
use Drupal\Core\Controller\ControllerBase; // Controlador base
use Drupal\node\Entity\Node; // Para usar entidades de tipo nodo
use Symfony\Component\HttpFoundation\RedirectResponse; // Para redirecciones


// Declaración de la clase
class UmbralController extends ControllerBase
{
	// Función para agregar o quitar de favoritos
	public function favoritos( $nid )
	{
		// Cargar el nodo
		$node = Node::load( $nid );
		
		// Si el nodo se cargó correctamente y es de tipo libro
		if( $node && $node -> bundle() == 'libro' )
		{
			// Obtener el uid del usuario actual
			$uid = \Drupal::currentUser() -> id();
			
			// Revisar si ese registro ya existe en la tabla
			$query = \Drupal::database() -> select( 'umbral_favoritos', 'u' );
			$query -> addExpression( 'COUNT( nid )' );
			$query -> condition( 'nid', $nid, '=' )
				-> condition( 'uid', $uid, '=' );
			
			// Guardar el resultado de la consulta
			$total = $query -> execute() -> fetchField();
			
			// Si no existe el registro
			if( $total == 0 ) 
			{
				// Agregar registro a la tabla de favoritos
				$query = \Drupal::database() -> insert( 'umbral_favoritos' )
					-> fields( array(
						'nid' => $nid,
						'uid' => $uid,
					) )
					-> execute();
			}
			
			// En caso contrario
			else
			{
				// Eliminar registro de favoritos
				$query = \Drupal::database() -> delete( 'umbral_favoritos' )
					-> condition( 'nid', $nid, '=' )
					-> condition( 'uid', $uid, '=' )
					-> execute();
			}
			
			// Redirigir
		}
		
		// Si llegamos aquí, termina el script
		$alias = \Drupal::service( 'path.alias_manager' ) -> getAliasByPath( '/node/' . $nid );
		$redirect = new RedirectResponse( $alias );
		$redirect -> send();
		exit();
	} 
}

?>