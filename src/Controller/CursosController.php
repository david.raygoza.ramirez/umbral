<?php

/**
 * Controlador principal del módulo
 */


// Declarar namespace
namespace Drupal\umbral\Controller;

// Importar clases
use Drupal\Core\Controller\ControllerBase; // Controlador base
use Drupal\node\Entity\Node; // Para usar entidades de tipo nodo
use Symfony\Component\HttpFoundation\RedirectResponse; // Para redirecciones
use Drupal\Core\Url;
// Declaración de la clase
class CursosController extends ControllerBase
{
    public function list(){
      $header_table = array(
         'id'=>   'Id',
        'name' => 'Nombre',
        'clave' => 'Clave',
        'opt' => t('operations'),
        'opt1' => t('operations'),
      );
      $query = \Drupal::database()->select('umbral_cursos', 'c');
      $query->fields('c', ['id','nombre','clave'])
      ->condition('active','1');
      $results = $query->execute()->fetchAll();
      $rows=[];
      foreach($results as $data){
        $deleteUrl=Url::fromRoute('curso.delete',['cid'=>$data->id],['absolute' => TRUE]);
        $urlForm=Url::fromRoute('cursos.add')->toString();
        $edit   = Url::fromUserInput($urlForm.'?id='.$data->id);
      //print the data from table
             $rows[] = [
              'id' =>$data->id,
              'name' => $data->nombre,
              'clave' => $data->clave,
                 \Drupal::l('Borrar', $deleteUrl),
                 \Drupal::l('Modificar', $edit),
            ];
      }

      $form['#markup'] = '<a href="'.$urlForm.'" class="button btn-primary">Agregar curso</a><br><br>';
      $form['#title'] = "Cursos";

      $form['table'] = [
            '#type' => 'table',
            '#header' => $header_table,
            '#rows' => $rows,
            '#empty' => t('No users found'),
      ];
       return $form;
    }
}
