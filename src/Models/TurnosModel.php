<?php
namespace Drupal\umbral\Models;
/**
 * DAO class for employee table.
 */
class TurnosModel {

    /**
     * To get multiple employee records.
     *
     * @param int $limit
     *   The number of records to be fetched.
     * @param string $orderBy
     *   The field on which the sorting to be performed.
     * @param string $order
     *   The sorting order. Default is 'DESC'.
     */
    public static function getAll($limit = null, $orderBy = null, $order = 'DESC',$active = 1)
    {
        $aDatos=[''=>'Seleccionar turno'];
        $query = \Drupal::database()
          ->select("umbral_turnos", 'e')
          ->condition('active', $active)
          ->fields('e');

        if ($limit) {
            $query->range(0, $limit);
        }
        if ($orderBy) {
            $query->orderBy($orderBy, $order);
        }

        $result = $query->execute()->fetchAll();
        foreach ($result as $key => $value) {
            $aDatos[$value->id ]= $value->nombre;
        }

          return $aDatos;
    }
}
