<?php
namespace Drupal\umbral\Models;
/**
 * DAO class for employee table.
 */
class EscuelasModel
{

    /**
     * To get multiple employee records.
     *
     * @param int $limit
     *   The number of records to be fetched.
     * @param string $orderBy
     *   The field on which the sorting to be performed.
     * @param string $order
     *   The sorting order. Default is 'DESC'.
     */

    public static function getAll($limit = null, $orderBy = null, $order = 'DESC', $active = 1)
    {
        $aDatos=[''=>'Seleccionar turno'];
        $query = \Drupal::database()
          ->select("umbral_escuelas", 'e')
          ->condition('active', $active)
          ->fields('e');

        if ($limit) {
            $query->range(0, $limit);
        }
        if ($orderBy) {
            $query->orderBy($orderBy, $order);
        }

        $result = $query->execute()->fetchAll();
        foreach ($result as $key => $value) {
            $aDatos[$value->id ]= $value->nombre;
        }

          return $aDatos;
    }
    public static function getByClave($clave = '')
    {
        $aDatos=[];
        $result = \Drupal::database()
        ->select("umbral_escuelas", 'e')
        ->condition('clave', $clave, '=')
        ->fields('e')
        ->execute()
        ->fetchField();
        return (bool) $result;
    }
    public static function getIdByClave($clave = '')
    {
        $result = \Drupal::database()
        ->select("umbral_escuelas", 'e')
        ->condition('active', 1)
        ->condition('clave', $clave, '=')
        ->fields('e')
        ->execute()
        ->fetchField();
        return  $result;
    }
}
