<?php
namespace Drupal\umbral\Forms\escuelas;

use Drupal;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\SafeMarkup;
use Drupal\Core\Url;


use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;

use Drupal\umbral\Models\EstadosModel;
use Drupal\umbral\Models\LocalidadesModel;
use Drupal\umbral\Models\TurnosModel;


use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\file\Entity\File;
use Drupal\Core\Database\Database;

class escuelaForm implements FormInterface
{
    private $table;
    private $routeList;
    private $label;
    private $labelPlural;
    private $routeAdd;
    public function __construct()
    {
        $this->table="umbral_escuelas";
        $this->routeList="umbral.escuelaslist";
        $this->label="Escuela";
        $this->labelPlural="Escuelas";
        $this->routeAdd="escuela.form";
    }
    public function getFormId()
    {
        return 'escuela_form';
    }
    /**
    * {@inheritdoc}
    */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $conn = Database::getConnection();
        $escuela = array();
        if (isset($_GET['id'])) {
            $query = $conn->select($this->table, 'm')
            ->condition('id', $_GET['id'])
            ->fields('m');
            $escuela = $query->execute()->fetchObject();
        }

        if ($escuela) {
            if ($escuela == 'invalid') {
                drupal_set_message(t('Registro invalido de escuela'), 'error');
                return new RedirectResponse(Drupal::url($this->routeList));
            }
            $form['id'] = [
              '#type' => 'hidden',
              '#value' => $escuela->id,
            ];
        }
        $form['#attributes']['novalidate'] = '';
        $form['general'] = [
          '#type' => 'details',
          "#title" => "Escuela",
          '#open' => true,
        ];
        $form['general']['name'] = [
          '#type' => 'textfield',
            '#title' => t('Nombre'),
            '#required' => true,
            '#size' => 60,
            '#maxlength' => 255,
            '#default_value' => ($escuela) ? $escuela->nombre : '',
        ];
        $form['general']['estados'] = [
          '#type' => 'select',
          '#title' => t('Estado'),
          '#options' => $this->getEstados(),
          '#required' => true,
          '#default_value' => ($escuela) ? $escuela->estado_id : '',
          '#ajax' => [
            'callback' => [$this, 'loadLocalidades'],
            'event' => 'change',
            'wrapper' => 'localidades',
          ],
        ];
        $changedEstado = $form_state->getValue('estados');
        if ($escuela) {
          if (!empty($changedEstado)) {
            $selectedEstado = $changedEstado;
          }
          else {
            $selectedEstado = $escuela->estado_id;
          }
        }
        else {
          $selectedEstado = $changedEstado;
        }
        $localidades = $this->getLocalidades($selectedEstado);
        $form['general']['localidades'] = [
            '#type' => 'select',
            '#prefix' => '<div id="localidades">',
            '#title' => t('State'),
            '#options' => $localidades,
            '#required' => true,
            '#suffix' => '</div>',
            '#default_value' => ($escuela) ? $escuela->municipio_id : '',
            '#validated' => true,
        ];
        $form['general']['colonia'] = [
          '#type' => 'textfield',
            '#title' => t('Domicilio'),
            '#required' => true,
            '#size' => 60,
            '#maxlength' => 255,
            '#default_value' => ($escuela) ? $escuela->colonia : '',
        ];
        $form['general']['turnos'] = [
          '#type' => 'select',
          '#title' => t('Turno'),
          '#options' => $this->getTurnos(),
          '#required' => true,
          '#default_value' =>  '',
          '#default_value' => ($escuela) ? $escuela->turno_id : '',
            '#suffix' => '</div>',
        ];
        $form['general']['actions'] = ['#type' => 'actions'];
        $form['general']['actions']['submit'] = [
            '#type' => 'submit',
            '#value' => 'Guardar',
          ];
        $form['general']['actions']['cancel'] = [
            '#type' => 'link',
            '#title' => 'Cancelar',
            '#attributes' => ['class' => ['button', 'button--primary']],
            '#url' => Url::fromRoute($this->routeList),
          ];
        return $form;
    }
    public function validateForm(array &$form, FormStateInterface $form_state)
    {
      //parent::validateForm($form, $form_state);
    }
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $conn = Database::getConnection();
        $field=$form_state->getValues();

        $name=$field['name'];
        $vendedorId=\Drupal::currentUser()->id();
        $estadoId = $field['estados'];
        $localidadId = $field['localidades'];
        $colonia = $field['colonia'];
        $turnoId = $field['turnos'];
        if (isset($_GET['id'])) {
            $field  = array(
             'nombre'   => $name,
             'vendedor_id'=> $vendedorId,
             'estado_id' => $estadoId,
             'municipio_id' => $localidadId,
             'colonia'  => $colonia,
             'turno_id' => $turnoId,
            );
            $query = \Drupal::database();
            $query->update($this->table)
             ->fields($field)
             ->condition('id', $_GET['id'])
             ->execute();
            drupal_set_message("Actualizado correctamente");
            $form_state->setRedirect($this->routeList);
        } else {
            $query = $conn->select($this->table, 'm');
            $query->addExpression('MAX(m.id)', 'maxval');
            $lastId = $query->execute()->fetchField();
            $lastId=str_pad($lastId, 5, "0", STR_PAD_LEFT);
            $fields  = array(
              'nombre'   =>  $name,
              'clave'   => base64_encode($lastId),
              'vendedor_id'=> $vendedorId,
              'estado_id' => $estadoId,
              'municipio_id' => $localidadId,
              'colonia'  => $colonia,
              'turno_id' => $turnoId,
              'active' => 1,
            );
          //  var_dump($fields);

            /*

            'localidad_id' => $localidadId,
            'colonia'  => $colonia,
            'turno_id' => $turnoId,
            */
            $query = \Drupal::database();
            $query ->insert($this->table)
              ->fields($fields)
              ->execute();
              drupal_set_message("Creado correctamente");
              $response = new RedirectResponse(\Drupal::url($this->routeList));
              $response->send();
        }
    }
    public function loadLocalidades(array &$form, FormStateInterface $form_state) {
        $form_state->setRebuild(TRUE);
        return $form['general']['localidades'];
    }
    public function getEstados()
    {
        return EstadosModel::getAll();
    }
    public function getLocalidades($selected)
    {
       return ($selected) ? LocalidadesModel::getAll($selected): ['' => 'Seleccionar estado'];
    }
    public function getTurnos()
    {
        return TurnosModel::getAll();
    }
}
