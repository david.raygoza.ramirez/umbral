<?php
namespace Drupal\umbral\Forms\escuelas;

use Drupal;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\SafeMarkup;
use Drupal\Core\Url;


use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;

use Drupal\umbral\Models\EstadosModel;
use Drupal\umbral\Models\LocalidadesModel;
use Drupal\umbral\Models\TurnosModel;
use Drupal\umbral\Models\EscuelasModel;


use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\file\Entity\File;
use Drupal\Core\Database\Database;
use Drupal\Core\Database\Connection;

class escuelaMaestroForm implements FormInterface
{
    private $table;
    private $routeList;
    private $label;
    private $labelPlural;
    private $routeAdd;
    public function __construct()
    {
        $this->table="umbral_de_escuela_profesor";
        $this->routeList="umbral.escuelasprofesor";
        $this->label="Escuela";
        $this->labelPlural="Escuelas";
        $this->routeAdd="escuela.form";
    }
    public function getFormId()
    {
        return 'escuela_form';
    }
    /**
    * {@inheritdoc}
    */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $conn = Database::getConnection();
        $escuela = array();
        if (isset($_GET['id'])) {
            $query = $conn->select($this->table, 'm')
            ->condition('id', $_GET['id'])
            ->fields('m');
            $escuela = $query->execute()->fetchObject();
        }

        if ($escuela) {
            if ($escuela == 'invalid') {
                drupal_set_message(t('Registro invalido de escuela'), 'error');
                return new RedirectResponse(Drupal::url($this->routeList));
            }
            $form['id'] = [
              '#type' => 'hidden',
              '#value' => $escuela->id,
            ];
        }
        $form['#attributes']['novalidate'] = '';
        $form['general'] = [
          '#type' => 'details',
          "#title" => "Escuela",
          '#open' => true,
        ];
        $form['general']['clave'] = [
          '#type' => 'textfield',
            '#title' => t('Clave'),
            '#required' => true,
            '#size' => 15,
            '#maxlength' => 20,
            '#default_value' => ($escuela) ? $escuela->nombre : '',
        ];

        $form['general']['actions'] = ['#type' => 'actions'];
        $form['general']['actions']['submit'] = [
            '#type' => 'submit',
            '#value' => 'Guardar',
          ];
        $form['general']['actions']['cancel'] = [
            '#type' => 'link',
            '#title' => 'Cancelar',
            '#attributes' => ['class' => ['button', 'button--primary']],
            '#url' => Url::fromRoute($this->routeList),
          ];
        return $form;
    }
    public function validateForm(array &$form, FormStateInterface $form_state)
    {
      //  parent::validateForm($form, $form_state);
        $values = $form_state->getValues();
        if (!EscuelasModel::getByClave($values['clave'])) {
            $form_state->setErrorByName('clave', 'Esta escuela no es permitida.'.$values['clave']);
        }
    }
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $conn = Database::getConnection();
        $field=$form_state->getValues();

        $profesorId=\Drupal::currentUser()->id();
        $claveEscuela = $field['clave'];
        $escuelaId=EscuelasModel::getIdByClave($field['clave']);
        if (isset($_GET['id'])) {
            $field  = array(
             'nombre'   => $name,
             'vendedor_id'=> $vendedorId,
             'estado_id' => $estadoId,
             'municipio_id' => $localidadId,
             'colonia'  => $colonia,
             'turno_id' => $turnoId,
            );
            $query = \Drupal::database();
            $query->update($this->table)
             ->fields($field)
             ->condition('id', $_GET['id'])
             ->execute();
            drupal_set_message("Actualizado correctamente");
            $form_state->setRedirect($this->routeList);
        } else {
            $query = $conn->select($this->table, 'm');
            $query->addExpression('MAX(m.id)', 'maxval');
            $lastId = $query->execute()->fetchField();
            $lastId=str_pad($lastId, 5, "0", STR_PAD_LEFT);
            $fields  = array(
              'escuela_id'   => $escuelaId,
              'profesor_id'=> $profesorId,
              'date_mod'=>date("Y-m-d H:i:s", time()),
            );
            $query = \Drupal::database();
            $query ->insert($this->table)
              ->fields($fields)
              ->execute();
              drupal_set_message("Creado correctamente");
              $response = new RedirectResponse(\Drupal::url($this->routeList));
              $response->send();
        }
    }
}
