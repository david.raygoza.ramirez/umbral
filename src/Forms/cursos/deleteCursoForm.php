<?php
namespace Drupal\umbral\Forms\cursos;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Url;
use Drupal\Core\Render\Element;

/**
 * Class DeleteForm.
 *
 * @package Drupal\mydata\Form
 */
class deleteCursoForm extends ConfirmFormBase
{
    public $id;
    private $table;
    private $listResource;
  /**
 * {@inheritdoc}
 */
    public function __CONSTRUCT()
    {
        $this->table="umbral_cursos";
        $this->listResource='umbral.cursoslist';
    }
    public function getFormId()
    {
        return 'delete_curso_form';
    }
    public function getQuestion()
    {
        return t('¿Quieres borrar el registro %id?', array('%id' => $this->id));
    }
    public function getCancelUrl()
    {
        return new Url($this->listResource);
    }
    public function getDescription()
    {
        return t('Solo si esta seguro.');
    }
    public function getConfirmText()
    {
        return t('Borrar');
    }
    public function getCancelText()
    {
        return t('Cancelar');
    }
    public function buildForm(array $form, FormStateInterface $form_state, $cid = null)
    {
        $this->id = $cid;
        return parent::buildForm($form, $form_state);
    }
    public function validateForm(array &$form, FormStateInterface $form_state)
    {
        parent::validateForm($form, $form_state);
    }
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
         $query = \Drupal::database();
         $query->update($this->table)
         ->fields(['active'=>0])
         ->condition('id', $this->id)
         ->execute();
         drupal_set_message("Borrado");
         $form_state->setRedirect($this->listResource);
    }
}
