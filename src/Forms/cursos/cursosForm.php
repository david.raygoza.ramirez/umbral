<?php
namespace Drupal\umbral\Forms\cursos;

use Drupal;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\SafeMarkup;
use Drupal\Core\Url;

use Drupal\employee\EmployeeStorage;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\file\Entity\File;
use Drupal\Core\Database\Database;

class cursosForm implements FormInterface {
  private $table;
  private $routeList;
  private $label;
  function __construct() {
       $this->table="umbral_cursos";
       $this->routeList="umbral.cursoslist";
       $this->label="Curso";
   }
  public function getFormId(){
    return 'cursos_form';
  }
  public function buildForm(array $form, FormStateInterface $form_state) {
      $conn = Database::getConnection();
      $curso = array();
      if (isset($_GET['id'])) {
          $query = $conn->select($this->table, 'm')
              ->condition('id', $_GET['id'])
              ->fields('m');
          $curso = $query->execute()->fetchObject();
      }
      if($curso){
          if ($curso == 'invalid') {
            drupal_set_message(t('Registro invalido de curso'), 'error');
            return new RedirectResponse(Drupal::url($this->$this->routeList));
          }
          $form['id'] = [
            '#type' => 'hidden',
            '#value' => $curso->id,
          ];
      }
      $form['#attributes']['novalidate'] = '';
      $form['general'] = [
        '#type' => 'details',
        "#title" => "Cursos",
        '#open' => TRUE,
      ];
      $form['general']['name'] = [
        '#type' => 'textfield',
          '#title' => t('Nombre'),
          '#required' => TRUE,
          '#default_value' => ($curso) ? $curso->nombre : '',
      ];
      $form['general']['descripcion'] = [
        '#type' => 'textarea',
          '#title' => t('Descripcion'),
          '#required' => TRUE,
          '#default_value' => ($curso) ? $curso->descripcion : '',
      ];
      $form['general']['ciclo_escolar'] = [
        '#type' => 'textfield',
          '#title' => t('Ciclo escolar'),
          '#required' => TRUE,
          '#default_value' => ($curso) ? $curso->ciclo_escolar : '',
      ];
      $form['actions'] = ['#type' => 'actions'];
      $form['actions']['submit'] = [
          '#type' => 'submit',
          '#value' => 'Guardar',
      ];
        $form['actions']['cancel'] = [
          '#type' => 'link',
          '#title' => 'Cancelar',
          '#attributes' => ['class' => ['button', 'button--primary']],
          '#url' => Url::fromRoute($this->routeList),
        ];
        return $form;
    }
    public function validateForm(array &$form, FormStateInterface $form_state) {
        //parent::validateForm($form, $form_state);
    }
    public function submitForm(array &$form, FormStateInterface $form_state) {
      $conn = Database::getConnection();
      $field=$form_state->getValues();
      $name=$field['name'];
      $cicloEscolar=$field['ciclo_escolar'];
      $description=$field['descripcion'];
      $profesorId=\Drupal::currentUser()->id();
      if (isset($_GET['id'])) {
          $field  = array(
              'nombre'   => $name,
              'profesor_id'=>$profesorId,
              'ciclo_escolar'=>$cicloEscolar,
              'descripcion'=>$description,
              'date_mod'=>date("Y-m-d H:i:s", time()),
          );
          $query = \Drupal::database();
          $query->update($this->table)
              ->fields($field)
              ->condition('id', $_GET['id'])
              ->execute();
          drupal_set_message("Actualizado correctamente");
          $form_state->setRedirect($this->routeList);
      }else{
          $query = $conn->select($this->table, 'm');
          $query->addExpression('MAX(m.id)', 'maxval');
          $lastId = $query->execute()->fetchField();
          $lastId=str_pad($lastId, 5, "0", STR_PAD_LEFT);
          $field  = array(
            'nombre'   => $name,
            'profesor_id'=>$profesorId,
            'ciclo_escolar'=>$cicloEscolar,
            'descripcion'=>$description,
            'date_mod'=>date("Y-m-d H:i:s", time()),

          );
           $query = \Drupal::database();
           $query ->insert($this->table)
               ->fields($field)
               ->execute();
           drupal_set_message("Creado correctamente");
           $response = new RedirectResponse(\Drupal::url($this->routeList));
           $response->send();
      }
    }
}
