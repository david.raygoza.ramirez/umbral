<?php

/**
 * Archivo para la creación del bloque de favoritos
 */

/**
 * @file
 * Contains \Drupal\umbral\Plugin\Block\FavsBlock
 */


// Declarar namespace
namespace Drupal\umbral\Plugin\Block;

// Importar clases
use Drupal\Core\Block\BlockBase; // Clase base para bloques
use Drupal\Core\Database\Database; // Para uso de la base de datos


/**
 * Crea un bloque
 *
 * @Block(
 * 	id = "umbral_favs_block",
 * 	admin_label = @Translation( "Umbral: Bloque de favoritos" ),
 * )
 *
 */
class FavsBlock extends BlockBase
{
	/**
	 * Función pública para construir el bloque
	 *
	 * {@inheritdoc}
	 *
	 */
	public function build()
	{
		// Obtener id del nodo actual
		$nid = \Drupal::routeMatch() -> getRawParameter( 'node' );
		
		// Obtener el uid del usuario actual
		$uid = \Drupal::currentUser() -> id();
		
		// Devuelve arreglo de contenido
		return array(
			'#cache' => array( 'max-age' => 0 ), // Para evitar caché
			'#theme' => 'template_favs_block',
			'#nid' => $nid,
			'#es_favorito' => umbral_es_favorito( $nid, $uid ),
		);
	}
}

?>