<?php

/**
 * Archivo con funciones para el módulo
 */


// Función para saber si un libro está ya en los favoritos de un usuario
function umbral_es_favorito( $nid, $uid )
{
	// Revisar si ese registro ya existe en la tabla
	$query = \Drupal::database() -> select( 'umbral_favoritos', 'u' );
	$query -> addExpression( 'COUNT( nid )' );
	$query -> condition( 'nid', $nid, '=' )
		-> condition( 'uid', $uid, '=' );
	
	// Guardar el resultado de la consulta
	$total = $query -> execute() -> fetchField();
	
	// Si ya existe el registro
	if( $total > 0 ) 
	{
		// Retornar verdadero
		return true;
	}
	
	// Si llegamos aquí, retornar falso
	return false;
}

?>