/**
 * Archivo de funciones de Javascript
 */


// Para utilizar jQuery
(function ( $ ) 
{
	// Inicio del script
	$( document ).ready( function()
	{ 

		/**
		 *  Para trabajar con el botón de envío de formulario de búsqueda de auditorías
		 */
		$( '#add-fav' ).click( function() 
		{
			ajaxCall( '/favoritos/' );	
		} );
		
		// Función para actualizar el valor de un elemento con AJAX
		function ajaxCall( url )
		{
			// Hacer la llamada a AJAX por JQuery
			$.ajax( {
				url: url,
				//data: 'variable=' + valorSeleccionado,
				type: 'get',
				/*success: function( result ) {
					$( idElementoAct ).html( result );
				}*/
			} );
		}

	} ); // Fin del Script
} ) ( jQuery );